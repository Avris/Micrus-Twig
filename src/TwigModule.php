<?php
namespace Avris\Micrus\Twig;

use Avris\Micrus\Bootstrap\ModuleInterface;
use Avris\Micrus\Bootstrap\ModuleTrait;

final class TwigModule implements ModuleInterface
{
    use ModuleTrait;
}

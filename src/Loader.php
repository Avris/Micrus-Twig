<?php
namespace Avris\Micrus\Twig;

use Twig\Loader\FilesystemLoader;

final class Loader extends FilesystemLoader
{
    protected function findTemplate($name, $throw = true)
    {
        return parent::findTemplate(strpos($name, '.') === false ? $name . '.html.twig' : $name);
    }
}

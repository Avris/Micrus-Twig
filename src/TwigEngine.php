<?php
namespace Avris\Micrus\Twig;

use Avris\Dispatcher\EventSubscriberInterface;
use Avris\Micrus\View\EngineInterface;
use Avris\Micrus\View\TemplateHelper;
use Symfony\Component\VarDumper\Cloner\VarCloner;
use Symfony\Component\VarDumper\Dumper\CliDumper;
use Symfony\Component\VarDumper\Dumper\HtmlDumper;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\TwigFunction;

final class TwigEngine implements EngineInterface, EventSubscriberInterface
{
    /** @var Environment */
    private $twig;

    /** @var string[] */
    private $dirs;

    public function __construct(
        array $dirs,
        TemplateHelper $helper,
        bool $envAppDebug,
        string $cacheDir,
        array $twigExtensions
    ) {
        $this->dirs = $dirs;
        $this->twig = new Environment(
            new Loader($dirs),
            [
                'cache' => $cacheDir,
                'debug' => $envAppDebug,
                'strict_variables' => true,
            ]
        );

        $this->customizeTwig($helper);

        foreach ($twigExtensions as $extension) {
            $this->twig->addExtension($extension);
        }
    }

    private function customizeTwig(TemplateHelper $helper)
    {
        $this->twig->addGlobal('app', $helper->getApp());

        $this->twig->addFunction(new TwigFunction(
            'route',
            [$helper, 'route'],
            ['is_safe' => ['html_attr']]
        ));

        $this->twig->addFunction(new TwigFunction(
            'routeExists',
            [$helper, 'routeExists']
        ));

        $this->twig->addFunction(new TwigFunction(
            'asset',
            [$helper, 'asset'],
            ['is_safe' => ['html_attr']]
        ));

        $this->twig->addFunction(new TwigFunction(
            'isGranted',
            [$helper, 'isGranted']
        ));

        $this->twig->addFunction(new TwigFunction(
            'canAccess',
            [$helper, 'canAccess']
        ));

        $cloner = new VarCloner();
        $htmlDumper = new HtmlDumper();
        $cliDumper = new CliDumper();
        $this->twig->addFunction(new TwigFunction(
            'dump',
            function ($var, $html = true) use ($cloner, $htmlDumper, $cliDumper) {
                $dumper = $html ? $htmlDumper : $cliDumper;

                return $dumper->dump($cloner->cloneVar($var), true);
            },
            ['is_safe' => ['html']]
        ));
    }

    public function render(string $template, array $vars): string
    {
        return $this->twig->render($template, $vars);
    }

    public function hasTemplate(string $template): bool
    {
        try {
            $this->twig->load($template);

            return true;
        } catch (LoaderError $e) {
            return false;
        }
    }

    public function getTwig(): Environment
    {
        return $this->twig;
    }

    public function warmup()
    {
        foreach ($this->dirs as $dir) {
            $it = new \RecursiveDirectoryIterator($dir, \RecursiveDirectoryIterator::SKIP_DOTS);
            $files = new \RecursiveIteratorIterator($it, \RecursiveIteratorIterator::SELF_FIRST);

            foreach ($files as $filename => $file) {
                if (!$file->isDir()) {
                    try {
                        $this->twig->load(substr($filename, strlen($dir) + 1));
                    } catch (LoaderError $e) {}
                }
            }
        }
    }

    public function getSubscribedEvents(): iterable
    {
        yield 'cacheWarmup' => [$this, 'warmup'];
    }
}

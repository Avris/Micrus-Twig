# Micrus Twig Bridge ##

This is a module for [Micrus framework](https://micrus.avris.it) that allows you
to integrate it with [Twig](http://twig.sensiolabs.org/) template engine.

## Installation

Run:

    composer require avris/micrus-twig

Then register the module in your `App\App:registerModules`: 

    yield new \Avris\Micrus\Twig\TwigModule;

## Usage    

Just put your `*.twig` templates in the `/templates` directory and render them in your controller.
If you don't specify an extension, `.html.twig` will be assumed:

    $this->render('Post/show', ['post' => $post]); // will render Post/show.html.twig

A twig global `app` is added, which gives you access to:

 * `app.user` (`null` if not logged in)
 * `app.flashBag`
 * `app.request`
 * `app.routeMatch`
 * `app.session`

Also some helper functions are provided:

 * `route('route', {params: value})`
 * `routeExists('route')`
 * `asset('asset.css')`
 * `isGranted('ROLE_ADMIN')`
 * `canAccess('check', object)`
 * `dump(object)`

## Extending Twig

To create Twig extension, please follow [its documentation](http://twig.sensiolabs.org/doc/advanced.html#creating-an-extension).

Any class extending `Twig\Extension\AbstractExtension` in an autoloaded directory
will be automatically registered as a Twig extension.
To do it manually, just declare it with a tag `twigExtension`:

    App\Service\MyTwigExtension:
      tags: [twigExtension]

## Copyright ###

* **Author:** Andrzej Prusinowski [(Avris.it)](https://avris.it)
* **Licence:** [MIT](https://mit.avris.it)
